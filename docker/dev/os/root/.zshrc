if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi

if [[ -o interactive ]]; then
exit
fi

cd $HOME
source .antigen.zsh

antigen use oh-my-zsh

antigen bundle command-not-found
antigen bundle git
antigen bundle git-extra
antigen bundle github
antigen bundle pip
antigen bundle zsh-users/zsh-autosuggestions
antigen bundle zsh-users/zsh-completions
antigen bundle zsh-users/zsh-history-substring-search
antigen bundle zsh-users/zsh-syntax-highlighting

antigen bundle skywind3000/z.lua
antigen theme robbyrussell

antigen apply

bindkey '^[[A' history-substring-search-up
bindkey '^[[B' history-substring-search-down

alias j=z

if [ -f ~/.tmux_default ]; then
. ~/.tmux_default > /dev/null 2>&1
fi

if [ -f ~/.bash_profile ]; then
. ~/.bash_profile
fi
