. $HOME/.asdf/asdf.sh
alias ls=exa
export EDITOR="vim"
export LANG="zh_CN.UTF-8"
export LANGUAGE="zh_CN:zh:en_US:en"
export TERM='xterm-256color'
export VISUAL="vim"
