#!/bin/bash

ROOT=$(cd "$(dirname "$0")"; pwd)

rsync -av $ROOT/os/ /
sed -i -r 's/Environment=DOCKER_SELINUX=--selinux-enabled=true/Environment=DOCKER_SELINUX=--selinux-enabled=false/' /run/systemd/system/docker.service

systemctl daemon-reload
systemctl enable docker.service
systemctl restart docker.service
# toolbox yum remove -y vi 
# toolbox yum install -y wget neovim tmux git 
# toolbox ln -s /usr/bin/nvim /usr/bin/vi 
# toolbox ln -s /usr/bin/nvim /usr/bin/vim

if [ ! -f "/opt/bin/static-get" ]; then
# REF: Sean Kane https://groups.google.com/d/msg/coreos-dev/JAeABXnQzuE/8v_SIPsnCAAJ
# create a directory 
sudo mkdir -p /opt/bin
sudo chown -R $(whoami):$(whoami) /opt
cd /opt/bin
wget https://raw.githubusercontent.com/minos-org/minos-static/master/static-get -O static-get
sudo sh -c 'echo "export PATH=$PATH:/opt/bin" >> /etc/environment'
source /etc/environment
cd /opt/bin
chmod a+rx ./static-get
fi

install () {
if [ ! -f "/opt/bin/$1" ]; then
echo "> INSTALL $1"
static-get -c
cd /opt
static-get $1
tar xf $1-*.tar.xz
rm -rf $1-*.xz
fi
}
install tmux 

if ! hash python3 2>/dev/null ; then
. $ROOT/python.sh
echo "export PATH=/opt/python/bin/:$PATH" >> .bash_profile
export PATH=/opt/python/bin/:$PATH
mkdir -p ~/.pip
echo -e "[global]\nindex-url = https://mirrors.aliyun.com/pypi/simple" > ~/.pip/pip.conf
fi

if ! hash docker-compose 2>/dev/null ; then
pip install docker-compose
fi

