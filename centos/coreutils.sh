#!/usr/bin/env bash
version=8.31
coreutils=coreutils-$version
wget -c http://ftp.gnu.org/gnu/coreutils/$coreutils.tar.xz
tar xfv $coreutils.tar.xz
rm -rf  $coreutils.tar.xz
cd $coreutils

FORCE_UNSAFE_CONFIGURE=1 ./configure \
--prefix=/usr \
--libexecdir=/usr/lib \
--enable-no-install-program=kill,uptime 

make -j48 

make install

yes | mv -v /usr/bin/{cat,chgrp,chmod,chown,cp,date,dd,df,echo,head,sleep,nice,false,ln,ls,mkdir,mknod,mv,pwd,rm,rmdir,stty,sync,true,uname,test,[} /bin 

yes | mv -v /usr/bin/chroot /usr/sbin

yes | mv -v /usr/share/man/man1/chroot.1 /usr/share/man/man8/chroot.8

sed -i s/\"1\"/\"8\"/1 /usr/share/man/man8/chroot.8

cd ..
rm -rf $coreutils
