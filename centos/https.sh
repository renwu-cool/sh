#!/usr/bin/env bash

ROOT=$(cd "$(dirname "$0")"; pwd)
cd $ROOT

HOST=$1



reloadcmd="systemctl force-reload nginx"

DNS=dns_dp
source  ../config/$DNS.sh

acme=$HOME/.acme.sh/acme.sh

if [ ! -x "$acme" ]; then
curl https://get.acme.sh | sh
fi

# export BRANCH=dev
# $acme --upgrade

if [ -f "$HOME/.acme.sh/$HOST/fullchain.cer" ]; then
echo "更新 $HOST"
$acme --use-wget --force --renew -d $HOST -d *.$HOST --log --reloadcmd "$reloadcmd"
else
echo "创建 $HOST"
$acme --use-wget --issue --dns $DNS -d $HOST -d *.$HOST --force --log --reloadcmd "$reloadcmd" 
chgrp www  ~/.acme.sh/ -R
chmod g+rx ~/.acme.sh/
chmod g+rx ~/.acme.sh/$HOST
chmod g+r ~/.acme.sh/$HOST/*
fi
