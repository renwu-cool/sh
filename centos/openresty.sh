#!/usr/bin/env bash
set -e

ROOT=$(cd "$(dirname "$0")"; pwd)
cd $ROOT

if [ ! -f "/usr/local/openresty/nginx/sbin/openresty" ]; then
exit 0
fi

yum install -y yum-utils
yum-config-manager --add-repo https://openresty.org/package/centos/openresty.repo
#yum-config-manager --add-repo https://openresty.org/package/amazon/openresty.repo
yum install -y openresty
yum install -y openresty-resty

setcap cap_net_bind_service=+eip /usr/local/openresty/nginx/sbin/nginx
cat nginx.conf > /usr/local/openresty/nginx/conf/nginx.conf

if [ ! -f "/usr/lib/systemd/system/nginx.service" ]; then
cp nginx.service /usr/lib/systemd/system/
systemctl enable nginx.service
systemctl start nginx.service
fi

if ! id -u www >/dev/null 2>&1; then
groupadd www
useradd -g www -s /sbin/nologin www
fi
mkdir -p /etc/nginx/conf.d
chown -R www:www /etc/nginx/
# PATH=/usr/local/openresty/nginx/sbin:$PATH
# export PATH
# alias acme.sh=~/.acme.sh/acme.sh
 
mkdir -p /var/log/nginx
sudo chown www:www /var/log/nginx

