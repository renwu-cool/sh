#!/usr/bin/env bash
set -e

ROOT=$(cd "$(dirname "$0")"; pwd)
cd $ROOT
./openresty.sh

while read line; do
[ -z "$line" ] && continue
./https.sh $line 
done < $ROOT/../config/host.txt

